// allow tests for invalid SSL
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

var
// libs needed for testing framework:
    assert = require('assert'),
    supertest = require('supertest'),// <<====== NOTE: not npm 'request' package
    expect = require('expect'),
    config = require('./config.json')
    ;

var request = supertest(config.url);

var paths_root = {
    main: "/"
};

var paths_api = {
    post_test: "/api/post-test"
};

//-------------------------------
// Tests below this section only
//-------------------------------

describe("(All tests:)\n", function () {


    it('GET ' + paths_root.main + ' must return 200 and match expected string', function (done) {
        request
            .get("/")
            .end(function (err, res) {
                expect(res.statusCode).toBe(200);
                expect(res.text).toEqual("Your Java server is up");
                done()
            });
    });

    it('POST ' + paths_api.post_test + ' return 200 and match expected string', function (done) {
        var testValue = "java";
        request
            .post(paths_api.post_test)
            .type("form")
            .send("testValue=" +testValue)
            .end(function (err, res) {
                expect(res.statusCode).toBe(200);
                expect(res.text).toEqual("The posted value is: " + testValue);
                done()
            });
    });

});