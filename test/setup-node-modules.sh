#!/usr/bin/env bash

echo "The mocha framework will be installed globally and your password is needed for this:"

sudo npm install -g mocha
npm install expect
npm install supertest
npm install assert