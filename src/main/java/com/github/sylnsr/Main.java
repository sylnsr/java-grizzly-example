/**
 * from documentation:
 * https://jersey.java.net/nonav/documentation/1.12/getting-started.html
 **/
package com.github.sylnsr;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.grizzly.http.server.HttpServer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.*;

@Path("/")
public class Main {

    private static HttpServer server;

    /**
     * main
     * @param args
     */
    public static void main(String[] args) throws IOException {

        int nextArg = 0;
        Config config = new Config();
        final Logger logger = new Logger();

        while (nextArg < args.length) {
            String nextArgVal = args[nextArg];

            if (nextArgVal.startsWith("-port:")) {
                config.port = (nextArgVal.split(":"))[1];
            } else if (nextArgVal.startsWith("-address:")) {
                config.address = (nextArgVal.split(":"))[1];
            } else if (nextArgVal.equals("-h")) {
                printHelpAndExit();
            }
            nextArg++;
        }

        // defaults
        if (config.port.equals("")) {
            logger.Info("Using default port 8080");
            config.port = "8080";
        }
        if (config.address.equals("")) {
            config.address = "0.0.0.0";
        }

        // Initiate the server
        String apiUrl = "http://" + config.address + ":" + config.port;
        ResourceConfig rc = new PackagesResourceConfig("com.github.sylnsr");
        rc.getProperties().put(
                "com.sun.jersey.spi.container.ContainerResponseFilters",
                "com.sun.jersey.api.container.filter.LoggingFilter");
        server = GrizzlyServerFactory.createHttpServer(apiUrl, rc);


        // Register the shutdown hook for the server
        Runtime.getRuntime().addShutdownHook(
                new Thread(
                        new Runnable() {
                            public void run() {
                                server.stop();
                                logger.Info("Server stopped");
                            }
                        },
                        "Server shutdownHook"
                )
        );

        // Run the server
        try {
            server.start();
            logger.Info("Endpoint = " + apiUrl);
            Thread.currentThread().join();

        } catch (Exception e) {
            logger.Fatal("There was an error while starting Grizzly HTTP server." + e.getMessage());
            System.exit(2);
        }
    }

    private static void printHelpAndExit() {
        System.out.println("Usage:");
        // todo on your own: print out some help info
        System.exit(-1);
    }

    @GET
    @Path("/")
    @Produces("text/plain")
    public String Root() {
        return "Your Java server is up";
    }

}