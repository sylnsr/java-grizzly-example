package com.github.sylnsr;

class Logger {

    private static void log(String s) {
        System.out.println(s);
    }

    public void Info(String s) {
        log("[info] " + s);
    }


    public void Fatal(String s) {
        log("[fatal] " + s);
    }


}