package com.github.sylnsr;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api")
public class API {

    @POST
    @Path("/post-test")
    @Produces("text/plain")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String PostTest(@FormParam("testValue") String testValue) {
        return "The posted value is: " + testValue;
    }
}
