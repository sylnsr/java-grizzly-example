#Java Grizzly Example

A sample shell project for working with REST in Java Grizzly (https://grizzly.java.net/)

## Build

	mvn package
	
## Run

	java -cp ./target/java-grizzly-example-1.0-SNAPSHOT.jar com.github.sylnsr.Main

## To Do
  - [ ] Dockerize
  - [ ] Add support for some common databases